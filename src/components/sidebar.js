//	Sidebar
import React from 'react';
import {render} from 'react-dom';
import { Link } from 'react-router'

export default class Sidebar extends React.Component<any, any> {

  render() {

    var userProfileData = this.props.userProfileData;
    var contact_photo = userProfileData.settings['contact.photo'],
        contact_firstname = userProfileData.settings['contact.firstname'],
        contact_lastname = userProfileData.settings['contact.lastname'];

    return(
      <div>
        {/* Left side column. contains the logo and sidebar */}
        <aside className="main-sidebar">
          {/* sidebar: style can be found in sidebar.less */}
          <section className="sidebar">
            {/* Sidebar user panel */}
            <div className="user-panel">
              <div className="pull-left image">
                <img src={ contact_photo } className="img-circle" alt="User Image" />
              </div>
              <div className="pull-left info">
                <p>{ contact_firstname } { contact_lastname }</p>
                <a href="#"><i className="fa fa-circle text-success"></i> Online</a>
              </div>
            </div>
            {/* search form */}
            <form action="#" method="get" className="sidebar-form">
              <div className="input-group">
                <input type="text" name="q" className="form-control" placeholder="Search..." />
                    <span className="input-group-btn">
                      <button type="submit" name="search" id="search-btn" className="btn btn-flat"><i className="fa fa-search"></i>
                      </button>
                    </span>
              </div>
            </form>
            {/* /.search form */}
            
            {/* sidebar menu: : style can be found in sidebar.less */}
            <ul className="sidebar-menu">
              <li className="header">MAIN NAVIGATION</li>
            {/*
              <li><Link to="/">My Profile</Link></li>
              <li><Link to="/settings">Settings</Link></li>
            */}
              <li className="active"><a href="./"><i className="fa fa-dashboard"></i> <span>My Profile</span></a></li>
              <li><a href="./settingsPage.html"><i className="fa fa-dashboard"></i> <span>Settings</span></a></li>
              <li><a href="#"><i className="fa fa-book"></i> <span>Logout</span></a></li>
            </ul>

          </section>
        </aside>
      </div>
    );

  }

}