//	Header
import React from 'react';
import {render} from 'react-dom';


class Header_Logo extends React.Component {

  render() {

    return(
      <a href="#" className="logo">
        {/* mini logo for sidebar mini 50x50 pixels */}
        <span className="logo-mini"><b>A</b>R</span>
        {/* logo for regular state and mobile devices */}
        <span className="logo-lg"><b>Address</b>REPORT</span>
      </a>
    );

  }

}

class Header_NavBar_UserMenu extends React.Component {

  render() {

    var userProfileData = this.props.userProfileData;
    console.debug( userProfileData );
    var contact_photo = userProfileData.settings['contact.photo'],
        contact_email = userProfileData.settings['contact.email'],
        contact_firstname = userProfileData.settings['contact.firstname'],
        contact_lastname = userProfileData.settings['contact.lastname'],
        contact_company = userProfileData.settings['contact.company'];

    return(
      <li className="dropdown user user-menu">
        <a href="#" className="dropdown-toggle" data-toggle="dropdown">
          <img src={ contact_photo } className="user-image" alt="User Image" />
          <span className="hidden-xs">{ contact_firstname } { contact_lastname }</span>
        </a>
        <ul className="dropdown-menu">
          {/* User image */}
          <li className="user-header">
            <img src={ contact_photo } className="img-circle" alt="User Image" />

            <p>
              { contact_firstname } { contact_lastname }
              <small>{ contact_company }</small>
            </p>
          </li>
          {/* Menu Body */}
          <li className="user-body">
            <div className="row">
              <div className="col-xs-4 text-center">
                <a href="#">Followers</a>
              </div>
              <div className="col-xs-4 text-center">
                <a href="#">Sales</a>
              </div>
              <div className="col-xs-4 text-center">
                <a href="#">Friends</a>
              </div>
            </div>
            {/* /.row */}
          </li>
          {/* Menu Footer */}
          <li className="user-footer">
            <div className="pull-left">
              <a href="#" className="btn btn-default btn-flat">Profile</a>
            </div>
            <div className="pull-right">
              <a href="#" className="btn btn-default btn-flat">Sign out</a>
            </div>
          </li>
        </ul>
      </li>
    );

  }

}

export default class Header extends React.Component<any, any> {

  render() {

    var userProfileData = this.props.userProfileData;

    return(
      <div>
        <header className="main-header">
          {/* Logo */}
          <Header_Logo />

          {/* Header Navbar: style can be found in header.less */}
          <nav className="navbar navbar-static-top">
            {/* Sidebar toggle button */}
            <a href="#" className="sidebar-toggle" data-toggle="offcanvas" role="button">
              <span className="sr-only">Toggle navigation</span>
            </a>

            <div className="navbar-custom-menu">
              <ul className="nav navbar-nav">

                {/* User Account: style can be found in dropdown.less */}
                <Header_NavBar_UserMenu userProfileData={ userProfileData } />

              </ul>
            </div>
          </nav>
        </header>
      </div>
    );

  }

}