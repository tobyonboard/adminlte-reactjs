//	NoMatch
import React from 'react';
import {render} from 'react-dom';

export default class NoMatch extends React.Component<any, any> {

  render() {

    return(
      <div>
       Not Found or No Matching Content
      </div>
    );

  }

}