//	Footer
import React from 'react';
import {render} from 'react-dom';

export default class Footer extends React.Component<any, any> {

  render() {

    return(
      <div>
        <footer className="main-footer">
          <div className="pull-right hidden-xs">
            <b>Version</b> 2016-09-01
          </div>
          <strong>Copyright &copy; { new Date().getFullYear() } <a href="http://www.addressreport.com">AddressReport</a></strong> ~ All rights
          reserved.
        </footer>
      </div>
    );

  }

}