//	MyProfile
import React from 'react';
import {render} from 'react-dom';

class Content_User_Profile_Photo extends React.Component {

  render(){

    var userProfileData = this.props.userProfileData;
    var contact_photo = userProfileData.settings['contact.photo'];

    return(
      <div>

        {/* About Me Box */}
        <div className="box box-default">
          <div className="box-header with-border">
            <h3 className="box-title">Profile Photo</h3>
          </div>
          {/* /.box-header */}
          <div className="box-body">
            <img src={ contact_photo } />
          </div>
          {/* /.box-body */}
        </div>
        {/* /.box */}

      </div>
    );

  }

}

class Content_User_Profile_Company_Logo extends React.Component {

  render(){

    var userProfileData = this.props.userProfileData;
    var contact_logo = userProfileData.settings['contact.logo'];

    return(
      <div>

        {/* About Me Box */}
        <div className="box box-default">
          <div className="box-header with-border">
            <h3 className="box-title">Company Logo</h3>
          </div>
          {/* /.box-header */}
          <div className="box-body">
            <img src={ contact_logo } />
          </div>
          {/* /.box-body */}
        </div>
        {/* /.box */}
        
      </div>
    );

  }

}

class Content_User_Profile_My_Account_Form extends React.Component {

  _update( x ){

    console.log( x );

  };

  render(){

    console.log( this.state );

    var userProfileData = this.props.userProfileData;
    var contact = {
      email : userProfileData.settings['contact.email'],
      firstname : userProfileData.settings['contact.firstname'],
      lastname : userProfileData.settings['contact.lastname'],
      phone : userProfileData.settings['contact.phone'],
      website : userProfileData.settings['contact.website'],
      license : userProfileData.settings['contact.license'],
    }

    return(
      <div>

        {/* form start */}
        <form className="form-horizontal" role="form">
          <div className="box-body">

            <div className="row">
              <div className="col-xs-6">

                <div className="form-group">
                  <label htmlFor="contact_firstname" className="col-sm-4 control-label">First Name</label>
                  <div className="col-sm-8">
                    <input type="text" className="form-control" id="contact_firstname" defaultValue={ contact.firstname } onChange={ this._update } placeholder="First name" />
                  </div>
                </div>

              </div>
              <div className="col-xs-6">

                <div className="form-group">
                  <label htmlFor="contact_lastname" className="col-sm-4 control-label">Last Name</label>
                  <div className="col-sm-8">
                    <input type="text" className="form-control" id="contact_lastname" defaultValue={ contact.lastname } onChange={ this._update } placeholder="First name" />
                  </div>
                </div>

              </div>
            </div>

            <div className="form-group">
              <label htmlFor="contact_email" className="col-sm-2 control-label">Email address</label>
              <div className="col-sm-10">
                <input type="email" className="form-control" id="contact_email" defaultValue={ contact.email } onChange={ this._update } placeholder="Enter email addrres" />
              </div>
            </div>

            <div className="form-group">
              <label htmlFor="contact_phone" className="col-sm-2 control-label">Phone Number</label>
              <div className="col-sm-10">
                <input type="tel" className="form-control" id="contact_phone" defaultValue={ contact.phone } onChange={ this._update } placeholder="Enter phone number" />
              </div>
            </div>

            <div className="form-group">
              <label htmlFor="contact_website" className="col-sm-2 control-label">Website</label>
              <div className="col-sm-10">
                <input type="url" className="form-control" id="contact_website" defaultValue={ contact.website } onChange={ this._update } placeholder="Enter website URL" />
              </div>
            </div>

            <div className="form-group">
              <label htmlFor="contact_license" className="col-sm-2 control-label">License</label>
              <div className="col-sm-10">
                <input type="text" className="form-control" id="contact_license" defaultValue={ contact.license } onChange={ this._update } placeholder="Enter website URL" />
              </div>
            </div>

          </div>
          {/* /.box-body */}

          <div className="box-footer">
            <button type="button" className="btn btn-primary" onClick={ this._update }>Submit</button>
          </div>
        </form>

      </div>
    );

  }

}

class Content_User_Profile_Company_Details_Form extends React.Component {

  _update( x ){

    console.log( x );

  };

  render(){

    var userProfileData = this.props.userProfileData;
    var contact_company  = userProfileData.settings['contact.company'],
        contact_office_address  = userProfileData.settings['contact.office'];

    return(
      <div>

        {/* form start */}
        <form className="form-horizontal" role="form">
          <div className="box-body">

            <div className="form-group">
              <label htmlFor="contact_company" className="col-sm-2 control-label">Company Name</label>
              <div className="col-sm-10">
                <input type="text" className="form-control" id="contact_company" defaultValue={ contact_company } onChange={ this._update } placeholder="Company name" />
              </div>
            </div>

            <div className="form-group">
              <label htmlFor="contact_office_address" className="col-sm-2 control-label">Company Address</label>
              <div className="col-sm-10">
                <input type="text" className="form-control" id="contact_office_address" defaultValue={ contact_office_address } onChange={ this._update } placeholder="Office address" />
              </div>
            </div>

          </div>
          {/* /.box-body */}

          <div className="box-footer">
            <button type="submit" className="btn btn-primary">Submit</button>
          </div>
        </form>

      </div>
    );

  }

}

class Content_User_Profile extends React.Component {

  render(){

    var userProfileData = this.props.userProfileData;

    return(
      <div>
        
        <div className="row">
          <div className="col-md-2">
            <div>
              <Content_User_Profile_Photo userProfileData={ userProfileData } />
            </div>
            <div>
              <Content_User_Profile_Company_Logo userProfileData={ userProfileData } />
            </div>
          </div>

          <div className="col-md-10">

            <div className="row">
              <div className="col-md-12">
                {/* Custom Tabs */}
                <div className="nav-tabs-custom">
                  <ul className="nav nav-tabs">
                    <li className="active"><a href="#tab_1" data-toggle="tab">Personal Information</a></li>
                    <li><a href="#tab_2" data-toggle="tab">Company Information</a></li>
                    <li><a href="#tab_3" data-toggle="tab">My App Settings</a></li>
                  </ul>
                  <div className="tab-content">
                    <div className="tab-pane active" id="tab_1">
                      <Content_User_Profile_My_Account_Form userProfileData={ userProfileData } />
                    </div>
                    {/* /.tab-pane */}
                    <div className="tab-pane" id="tab_2">
                      <Content_User_Profile_Company_Details_Form userProfileData={ userProfileData } />
                    </div>
                    {/* /.tab-pane */}
                    <div className="tab-pane" id="tab_3">
                      APP SETTINGS FORM HERE
                    </div>
                    {/* /.tab-pane */}
                  </div>
                  {/* /.tab-content */}
                </div>
                {/* nav-tabs-custom */}
              </div>
              {/* /.col */}
            </div>

          </div>
        </div>

      </div>
    );

  }

}

export default class Content_Profile extends React.Component<any, any> {

  render() {

    var userProfileData = this.props.userProfileData;

    return(
      <div>
        <div className="content-wrapper">

          {/* Content Header (Page header) */}
          <section className="content-header">
            <h1>
              Dashboard
              <small>My Profile Panel</small>
            </h1>
            <ol className="breadcrumb">
              <li><a href="#"><i className="fa fa-dashboard"></i> Home</a></li>
              <li className="active">Dashboard</li>
            </ol>
          </section>

          {/* Main content */}
          <section className="content">

            <Content_User_Profile userProfileData={ userProfileData } />

            <hr />
            <pre className="debug-box">{ JSON.stringify( userProfileData, null, 2 )}</pre>

          </section>

        </div>
      </div>
    );

  }

}