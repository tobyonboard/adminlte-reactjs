//	Settings
import React from 'react';
import {render} from 'react-dom';

export default class Content extends React.Component<any, any> {

  render() {

    var userProfileData = this.props.userProfileData;

    return(
      <div>
        <div className="content-wrapper">

          {/* Content Header (Page header) */}
          <section className="content-header">
            <h1>
              Dashboard
              <small>SETTINGS</small>
            </h1>
            <ol className="breadcrumb">
              <li><a href="#"><i className="fa fa-dashboard"></i> Home</a></li>
              <li className="active">Dashboard</li>
            </ol>
          </section>

          {/* Main content */}
          <section className="content">

            <Content_User_Profile userProfileData={ userProfileData } />

            <hr />
            <pre className="debug-box">{ JSON.stringify( userProfileData, null, 2 )}</pre>

          </section>

        </div>
      </div>
    );

  }

}