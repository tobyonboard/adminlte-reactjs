//	index
import React from 'react';
import {render} from 'react-dom';
import Header from './components/header';
import Sidebar from './components/sidebar';
import Content_Profile from './components/content/myprofile';
import Settings from './components/content/settings';
import NoMatch from './components/nomatch';
import Footer from './components/footer';

var $ = require ('jquery');

class App extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            userProfileData: {
               'settings': {}
            },
        };
    }

    componentDidMount() {
        var voila = this;

    //  fire an ajax request here
    //  get userProfileData from AJAX
        var jsonData = {
            "api_key" : "e0d73422-27c2-4604-b28a-02bd9b9468ff",
            "api_secret" : "959316b5-52f6-4fd9-a3b0-007755207298",
            "target_api_key" : "9b01508a-20b1-460a-ad84-5a86c73bd367",
            "infoscope" : "basic",
            "formBaseURL" : "https://demopartner-dot-staging-dot-rentenna3.appspot.com"
        };
        console.log( 'jsonData', jsonData );

        var userProfileData = $.ajax({
            url: "http://code-test.ten80snowboarder.com/adminLTE/ajaxLoader/getPartnerProfile.php",
            data: jsonData,
            type: 'POST',
            dataType: 'json',
            success: function( rslt ){
                console.log( 'UPDATED STATE', this.state.data );
            /*
                voila.setState(
                    { data: JSON.parse( rslt ) },
                    function() {
                    //  console.log( 'UPDATED STATE', this.state.data )
                    });
                */
            }.bind(this),
            error: function( xhr, status, err ){
                console.error( this.props, status, err.toString() );
            }.bind(this)
        });

    }

  render() {

/*    var userProfileData = {
      "api_key": "9b01508a-20b1-460a-ad84-5a86c73bd367",
      "localSettings": {
        "contact.company": "Onboard Informatics",
        "contact.email": "spetronis@onboardinformatics.com",
        "contact.firstname": "Scott",
        "contact.lastname": "Petronis",
        "contact.license": "DUCHESS",
        "contact.logo": "http://onboardinformatics.com/wp-content/uploads/2015/09/logo.png",
        "contact.office": "",
        "contact.phone": "1.646.747.4269",
        "contact.photo": "https://demopartner-dot-staging-dot-rentenna3.appspot.com/subdomain-image/db210787-00e7-4874-80a5-5ff830428dd0/",
        "contact.website": "http://onboardinformatics.com",
        "preferredDomain": "https://demopartner-dot-staging-dot-rentenna3.appspot.com"
      },
      "name": "scott petronis",
      "pid": "scottp",
      "settings": {
        "alerts": true,
        "branding.cobranding": true,
        "branding.position": "right",
        "contact.company": "Onboard Informatics",
        "contact.email": "spetronis@onboardinformatics.com",
        "contact.firstname": "Scott",
        "contact.lastname": "Petronis",
        "contact.license": "DUCHESS",
        "contact.logo": "http://onboardinformatics.com/wp-content/uploads/2015/09/logo.png",
        "contact.office": "",
        "contact.phone": "1.646.747.4269",
        "contact.photo": "https://demopartner-dot-staging-dot-rentenna3.appspot.com/subdomain-image/db210787-00e7-4874-80a5-5ff830428dd0/",
        "contact.website": "http://onboardinformatics.com",
        "createSubPartner": false,
        "enableBreadcrumbs": true,
        "headerColor": "light",
        "leadDistribution": true,
        "leadTargets": null,
        "logo": "/subdomain-image/68e0f67d-3c34-473b-8ae7-bb48ec750025/",
        "nav": true,
        "preferredDomain": "https://demopartner-dot-staging-dot-rentenna3.appspot.com",
        "renderFullReports": false,
        "state.AK": true,
        "state.AL": true,
        "state.AR": true,
        "state.AZ": true,
        "state.CA": true,
        "state.CO": true,
        "state.CT": true,
        "state.DC": true,
        "state.DE": true,
        "state.FL": true,
        "state.GA": true,
        "state.HI": true,
        "state.IA": true,
        "state.ID": true,
        "state.IL": true,
        "state.IN": true,
        "state.KS": true,
        "state.KY": true,
        "state.LA": true,
        "state.MA": true,
        "state.MD": true,
        "state.ME": true,
        "state.MI": true,
        "state.MN": true,
        "state.MO": true,
        "state.MS": true,
        "state.MT": true,
        "state.NC": true,
        "state.ND": true,
        "state.NE": true,
        "state.NH": true,
        "state.NJ": true,
        "state.NM": true,
        "state.NV": true,
        "state.NY": true,
        "state.OH": true,
        "state.OK": true,
        "state.OR": true,
        "state.PA": true,
        "state.PR": true,
        "state.RI": true,
        "state.SC": true,
        "state.SD": true,
        "state.TN": true,
        "state.TX": true,
        "state.UT": true,
        "state.VA": true,
        "state.VT": true,
        "state.WA": true,
        "state.WI": true,
        "state.WV": true,
        "state.WY": true
      },
      "status": "active"
    }*/

    return (
        <div>
            <Header userProfileData={ this.state.userProfileData } />
            <Sidebar userProfileData={ this.state.userProfileData } />
            <Content_Profile userProfileData={ this.state.userProfileData } />
            <Footer userProfileData={ this.state.userProfileData } />
        </div>
    );
  }
}

render(
	<App />,
	document.querySelector( '#app' )
);